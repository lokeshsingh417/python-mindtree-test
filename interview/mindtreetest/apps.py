from django.apps import AppConfig


class MindtreetestConfig(AppConfig):
    name = 'mindtreetest'
