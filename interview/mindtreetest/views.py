from django.shortcuts import render
from rest_framework import viewsets
from .serializer import RouterSerializer
from .models import Router
from django.http import JsonResponse
# Create your views here.
import json
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import IsAuthenticated

class RouterViewSet(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]
    def list(self, request):
        routers=Router.objects.filter(isDeleted=False)
        response=RouterSerializer(routers,many=True)
        return Response(response.data)


    def create(self, request):
        payload=RouterSerializer(data=request.data)
        if(payload.is_valid()):
            payload.save()
            print("saved")
            return Response(payload.data)
        else:
            return Response({
                    "status":"failure",
                    "message":self.get_first_error_from_serializer(payload)
                })


    def retrieve(self, request, pk=None):
        routers=Router.objects.filter(id=pk,isDeleted=False)
        if(routers.exists()):
            response=RouterSerializer(routers[0])
            return Response(response.data)
        else:
            return Response({
                    "status":"failure",
                    "message":self.get_first_error_from_serializer(payload)
                })



    def get_first_error_from_serializer(self,serializer):
        """
        get first error from a serializer
        :param serializer:
        :return: first error of serializer
        """
        error_message = ''
        for name, message in serializer.errors.items():
            error_message = name + ' - ' + message[0]
            break

        return error_message

    def update(self, request, pk=None):
        try:
            router=Router.objects.get(id=pk,isDeleted=False)
            payload=RouterSerializer(router,data=request.data)
            if(payload.is_valid()):
                payload.save()
                return Response(payload.data)
            else:
                return Response({
                    "status":"failure",
                    "message":self.get_first_error_from_serializer(payload)
                })


        except BaseException as e:
            return Response({
                "status":"failure",
                "message":"Router Not Available"
            })
        

    def partial_update(self, request, pk=None):
        try:
            router=Router.objects.get(id=pk,isDeleted=False)
            payload=RouterSerializer(router,data=request.data)
            if(payload.is_valid()):
                payload.save()
                return Response(payload.data)
            else:
                return Response({
                    "status":"failure",
                    "message":self.get_first_error_from_serializer(payload)
                })


        except BaseException as e:
            return Response({
                "status":"failure",
                "message":"Router Not Available"
            })
        
    def destroy(self, request, pk=None):
        try:
            router=Router.objects.get(id=pk,isDeleted=False)
            router.isExpired=True
            router.save()
            return Response({
                    "status":"success",
                "message":f"Router {router.Hostname} Deleted"
            
            })

        except BaseException as e:
            return Response({
                "status":"failure",
                "message":"Router Not Available"
            })
        