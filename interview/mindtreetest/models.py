from django.db import models

# Create your models here.


# model for Routers

class Router(models.Model):
    Sapid = models.CharField(max_length=18)
    Hostname = models.CharField(max_length=14)
    Loopback = models.GenericIPAddressField()
    MacAddress = models.CharField(max_length=17)
    isDeleted=models.BooleanField(default=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    modified = models.DateTimeField(auto_now=True, auto_now_add=False)
    

    def __str__(self):
        return f"{self.Hostname} {self.Loopback}"
    