import random
import requests
from faker import Faker  
class Generator():
    def __init__(self,count):
        self.count = count

    def generate(self):
        for i in range(self.count):
            Sapid=self.get_random_alphabets(18)
            Hostname=self.get_random_alphabets(14)
            Loopback =Faker().ipv4()
            MacAddress=self.randomMacAddress()
            url = "http://127.0.0.1:8000"
            # headers = {"Content-Type":"application/json"}
            payload = {"Sapid":Sapid,"Hostname":Hostname,"Loopback":Loopback,"MacAddress":MacAddress} 
            print(payload)   
            response=requests.post(url,payload) 
            # print(response)
            print(response.status_code)

    def randomMacAddress(self):
        return "02:00:00:%02x:%02x:%02x" % (random.randint(0, 255),
                             random.randint(0, 255),
                             random.randint(0, 255))

    def get_random_alphabets(self,length):
        """
        get random alphabets of length
        :param length:
        :return:
        """
        return ''.join(random.choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ") for i in range(length))


if __name__ == "__main__":
    no=int(input("Enter No !!"))
    g=Generator(no)
    g.generate()